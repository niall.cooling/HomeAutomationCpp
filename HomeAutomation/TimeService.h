//
//  TimeService.h
//  HomeAutomation
//
//  Created by Niall Cooling on 23/01/2014.
//  Copyright (c) 2014 Niall Cooling. All rights reserved.
//

#ifndef HomeAutomation_TimeService_h
#define HomeAutomation_TimeService_h

class I_WakeUp;

class I_TimeService
{
public:
    struct Time
    {
        int minuteOfDay;
        int dayOfWeek;
    };
    
    virtual void SetPeriodicAlarmInSeconds(int seconds, I_WakeUp& callback) = 0;
    virtual void CancelPeriodicAlarmInSeconds(int seconds, I_WakeUp& callback) = 0;
    virtual Time GetTime() = 0;
};

class I_WakeUp
{
public:
    virtual void Wakeup(I_TimeService::Time time) = 0;
};

#endif
