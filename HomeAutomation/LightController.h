//
//  LightController.h
//  HomeAutomation
//
//  Created by Niall Cooling on 23/01/2014.
//  Copyright (c) 2014 Niall Cooling. All rights reserved.
//

#ifndef HomeAutomation_LightController_h
#define HomeAutomation_LightController_h


class I_LightController
{
public:
    virtual void On(unsigned id) = 0;
    virtual void Off(unsigned id) = 0;
    virtual unsigned maxLights() = 0;
};

#endif
