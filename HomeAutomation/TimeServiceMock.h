//
//  TimeServiceMock.h
//  HomeAutomation
//
//  Created by Niall Cooling on 23/01/2014.
//  Copyright (c) 2014 Niall Cooling. All rights reserved.
//

#ifndef __HomeAutomation__TimeServiceMock__
#define __HomeAutomation__TimeServiceMock__

#include "TimeService.h"

#include "gmock/gmock.h"  // Brings in Google Mock.

class MockTimeService: public I_TimeService
{
public:
    MOCK_METHOD2(SetPeriodicAlarmInSeconds, void(int seconds, I_WakeUp& callback));
    MOCK_METHOD2(CancelPeriodicAlarmInSeconds, void(int seconds, I_WakeUp& callback));
    MOCK_METHOD0(GetTime, Time(void));
};


#endif /* defined(__HomeAutomation__TimeServiceMock__) */
