//
//  LightScheduler.cpp
//  HomeAutomation
//
//  Created by Niall Cooling on 23/01/2014.
//  Copyright (c) 2014 Niall Cooling. All rights reserved.
//
#include "LightScheduler.h"
#include "LightController.h"


LightScheduler::LightScheduler(I_TimeService& ts, I_LightController& lc):
    myTimeService(ts),
    myLightController(lc)
{
    for (unsigned i = 0; i < MAX_EVENTS; i++) {
        scheduledEvents[i].id = UNUSED;
    }
    myTimeService.SetPeriodicAlarmInSeconds(60,
                                          *(this));
}

LightScheduler::~LightScheduler()
{
    myTimeService.CancelPeriodicAlarmInSeconds(60,
                                             *(this));
}


LightScheduler::result_t LightScheduler::ScheduleTurnOn(unsigned id, day_t day, unsigned minute)
{
    return scheduleEvent(id, day, minute, TURN_ON);
}

LightScheduler::result_t LightScheduler::ScheduleTurnOff(unsigned id, day_t day, unsigned minute)
{
    return scheduleEvent(id, day, minute, TURN_OFF);
}

LightScheduler::result_t LightScheduler::scheduleEvent(unsigned id, day_t day, unsigned minute, event_t event)
{
    if (id >= myLightController.maxLights())
        return ID_OUT_OF_BOUNDS;
    
    for (unsigned i = 0; i < MAX_EVENTS; i++)
    {
        if (scheduledEvents[i].id == UNUSED)
        {
            scheduledEvents[i].time.dayOfWeek = day;
            scheduledEvents[i].time.minuteOfDay = minute;
            scheduledEvents[i].event = event;
            scheduledEvents[i].id = id;
            return OK;
        }
    }
    return TOO_MANY_EVENTS;
}

LightScheduler::result_t LightScheduler::ScheduleRemove(unsigned id, day_t day, unsigned minute)
{
    bool found = false;
    
    for (unsigned i = 0; i < MAX_EVENTS; i++)
    {
        if (scheduledEvents[i].id == id
            && scheduledEvents[i].time.dayOfWeek == day
            && scheduledEvents[i].time.minuteOfDay == minute)
        {
            scheduledEvents[i].id = UNUSED;
            found = true;
        }
    }
    
    if(found) {
        return OK;
    }
    else {
        return ID_NOT_FOUND;
    }
}

void LightScheduler::Wakeup(I_TimeService::Time time)
{
    for (unsigned i = 0; i < MAX_EVENTS; i++)
    {
        processEventDueNow(time, scheduledEvents[i]);
    }
}

void LightScheduler::processEventDueNow(I_TimeService::Time& time, ScheduledLightEvent& lightEvent)
{
    if (lightEvent.id == UNUSED) {
        return;
    }
    if (!DoesLightRespondToday(time, lightEvent.time.dayOfWeek)) {
        return;
    }
    if (lightEvent.time.minuteOfDay != time.minuteOfDay) {
        return;
    }
    operateLight(lightEvent);
}

bool LightScheduler::DoesLightRespondToday(I_TimeService::Time& time, int reactionDay)
{
    int today = time.dayOfWeek;
    
    if (reactionDay == EVERYDAY) {
        return true;
    }
    if (reactionDay == today) {
        return true;
    }
    if (reactionDay == WEEKEND && (today == SATURDAY || today == SUNDAY)) {
        return true;
    }
    if (reactionDay == WEEKDAY && today >= MONDAY && today <= FRIDAY) {
        return true;
    }
    return false;
}

void LightScheduler::operateLight(ScheduledLightEvent& lightEvent)
{
    if (lightEvent.event == TURN_ON) {
        myLightController.On(lightEvent.id);
    }
    else if (lightEvent.event == TURN_OFF) {
        myLightController.Off(lightEvent.id);
    }
}








/////////////////////////////////////////////////////////////////////////////////////////////////////////////

#if 0
enum
{
    UNUSED = -1,
    TURN_OFF, TURN_ON,
};

typedef struct
{
    int id;
    int minuteOfDay;
    int event;
    
} ScheduledLightEvent;

static ScheduledLightEvent scheduledEvent;

void LightScheduler_Create(void)
{
    scheduledEvent.id = UNUSED;
    
}
#endif

#if 0
static ScheduledLightEvent scheduledEvent;
static ScheduledLightEvent scheduledEvents[MAX_EVENTS];

void LightScheduler_Create(void)
{
    int i;
    
    scheduledEvent.id = UNUSED;
    
    for (i = 0; i < MAX_EVENTS; i++)
        scheduledEvents[i].id = UNUSED;
    TimeService_SetPeriodicAlarmInSeconds(60,
                                          LightScheduler_Wakeup);
}
#endif

#if 0
static ScheduledLightEvent scheduledEvent;
void LightScheduler_Create(void)
{
    int i;
    scheduledEvent.id = UNUSED;
    
    TimeService_SetPeriodicAlarmInSeconds(60,
                                          LightScheduler_Wakeup);
}
#endif

#if 0
void LightScheduler_Destroy(void)
{
}
#endif

#if 0
static void scheduleEvent(int id, Day day, int minuteOfDay, int event)
{
    int i;
    
    for (i = 0; i < MAX_EVENTS; i++)
    {
        if (scheduledEvents[i].id == UNUSED)
        {
            scheduledEvents[i].day = day;
            scheduledEvents[i].minuteOfDay = minuteOfDay;
            scheduledEvents[i].event = event;
            scheduledEvents[i].id = id;
            break;
        }
    }
    
    scheduledEvent.day = day;
    scheduledEvent.minuteOfDay = minuteOfDay;
    scheduledEvent.event = event;
    scheduledEvent.id = id;
}
#endif

#if 0
static void scheduleEvent(int id, Day day, int minuteOfDay, int event)
{
    scheduledEvent.minuteOfDay = minuteOfDay;
    scheduledEvent.event = event;
    scheduledEvent.id = id;
}
#endif

#if 0
static void scheduleEvent(int id, Day day, int minuteOfDay, int event)
{
    int i;
    
    for (i = 0; i < MAX_EVENTS; i++)
    {
        if (scheduledEvents[i].id == UNUSED)
        {
            scheduledEvents[i].day = day;
            scheduledEvents[i].minuteOfDay = minuteOfDay;
            scheduledEvents[i].event = event;
            scheduledEvents[i].id = id;
            break;
        }
    }
}

void LightScheduler_ScheduleTurnOn(int id, Day day, int minuteOfDay)
{
    scheduleEvent(id, day, minuteOfDay, TURN_ON);
}

void LightScheduler_ScheduleTurnOff(int id, Day day, int minuteOfDay)
{
    scheduleEvent(id, day, minuteOfDay, TURN_OFF);
}
#endif

#if 0
void LightScheduler_Wakeup(void)
{
    Time time;
    TimeService_GetTime(&time);
    processEventDueNow(&time, &scheduledEvent);
}
#endif

#if 0
void LightScheduler_Wakeup(void)
{
    int i;
    Time time;
    TimeService_GetTime(&time);
    
    for (i = 0; i < MAX_EVENTS; i++)
    {
        processEventDueNow(&time, &scheduledEvents[i]);
    }
    
    processEventDueNow(&time, &scheduledEvent);
}
#endif

#if 0
void LightScheduler_Wakeup(void)
{
    int i;
    
    Time time;
    TimeService_GetTime(&time);
    
    for (i = 0; i < MAX_EVENTS; i++)
    {
        processEventDueNow(&time, &scheduledEvents[i]);
    }
}
#endif

#if 0
void LightScheduler_Wakeup(void)
{
    int i;
    Time time;
    TimeService_GetTime(&time);
    
    for (i = 0; i < MAX_EVENTS; i++)
    {
        processEventDueNow(&time, &scheduledEvents[i]);
    }
    
    processEventDueNow(&time, &scheduledEvent);
}
#endif

#if 0
void LightScheduler_ScheduleRemove(int id, Day day, int minuteOfDay)
{
    if (scheduledEvent.id == id
        && scheduledEvent.day == day
        && scheduledEvent.minuteOfDay == minuteOfDay)
    {
        scheduledEvent.id = UNUSED;
    }
}
#endif

#if 0
static ScheduledLightEvent scheduledEvent;

static int DoesLightRespondToday(Time * time, int reactionDay)
{
    int today = time->dayOfWeek;
	
    if (reactionDay == EVERYDAY)
        return TRUE;
    
    if (reactionDay == today)
        return TRUE;
    if (reactionDay == WEEKEND && (SATURDAY == today || SUNDAY == today))
        return TRUE;
    
    if (reactionDay == WEEKDAY && today >= MONDAY && today <= FRIDAY)
        return TRUE;
    
    return FALSE;
}
#endif

#if 0
static void processEventDueNow(Time * time, ScheduledLightEvent * lightEvent)
{
    int reactionDay = lightEvent->day;
    if (lightEvent->id == UNUSED)
        return;
    if (reactionDay != EVERYDAY && reactionDay != today)
        return;
    if (lightEvent->minuteOfDay != time->minuteOfDay)
        return;
    
    operateLight(lightEvent);
}
#endif

#if 0
static void processEventDueNow(Time * time, ScheduledLightEvent * lightEvent)
{
    if (lightEvent->id == UNUSED)
        return;
    if (lightEvent->day != EVERYDAY)
        return;
    if (lightEvent->minuteOfDay != time->minuteOfDay)
        return;
    
    operateLight(lightEvent);
}
#endif

#if 0
static void operateLight(ScheduledLightEvent * lightEvent)
{
    if (lightEvent->event == TURN_ON)
        LightController_On(lightEvent->id);
    else if (lightEvent->event == TURN_OFF)
        LightController_Off(lightEvent->id);
}

static void processEventDueNow(Time * time, ScheduledLightEvent * lightEvent)
{
    if (lightEvent->id == UNUSED)
        return;
    
    if (lightEvent->minuteOfDay != time->minuteOfDay)
        return;
    
    operateLight(lightEvent);
}

void LightScheduler_Wakeup(void)
{
    Time time;
    TimeService_GetTime(&time);
    
    processEventDueNow(&time, &scheduledEvent);
}

#endif

#if 0
static ScheduledLightEvent scheduledEvent;
void LightScheduler_ScheduleTurnOn(int id, Day day, int minuteOfDay)
{
    scheduledEvent.minuteOfDay = minuteOfDay;
    scheduledEvent.event = TURN_ON;
    scheduledEvent.id = id;
}

void LightScheduler_ScheduleTurnOff(int id, Day day, int minuteOfDay)
{
    scheduledEvent.minuteOfDay = minuteOfDay;
    scheduledEvent.event = TURN_OFF;
    scheduledEvent.id = id;
}

void LightScheduler_Wakeup(void)
{
    Time time;
    TimeService_GetTime(&time);
    if (scheduledEvent.id == UNUSED)
        return;
    if (time.minuteOfDay != scheduledEvent.minuteOfDay)
        return;
    if (scheduledEvent.event == TURN_ON)
        LightController_On(scheduledEvent.id);
    
    else if (scheduledEvent.event == TURN_OFF)
        LightController_Off(scheduledEvent.id);
}
#endif

#if 0
typedef struct
{
    int id;
    int minuteOfDay;
} ScheduledLightEvent;

static ScheduledLightEvent scheduledEvent;

void LightScheduler_Create(void)
{
    scheduledEvent.id = UNUSED;
}
#endif

#if 0
void LightScheduler_ScheduleTurnOn(int id, Day day, int minuteOfDay)
{
    scheduledEvent.id = id;
    scheduledEvent.minuteOfDay = minuteOfDay;
}
void LightScheduler_Wakeup(void)
{
    Time time;
    TimeService_GetTime(&time);
	
    if (scheduledEvent.id == UNUSED)
        return;
    if (time.minuteOfDay != scheduledEvent.minuteOfDay)
        return;
    
    LightController_On(scheduledEvent.id);
}
#endif 

#if 0 
void LightScheduler_ScheduleTurnOn(int id, Day day, int minuteOfDay)
{
}

void LightScheduler_Wakeup(void)
{
}
#endif 
