[ ![Codeship Status for nscooling/homeautomationc](https://app.codeship.com/projects/bdc50570-bfc5-0134-342f-0ea17c17ebe1/status?branch=master)](https://app.codeship.com/projects/196721)

This code is built to demonstrate googletest (gtest) and google mock (gmock).
The current project format is an Xcode project, but all based on g++ (well clang on the Mac)

It is based on the code foiund in the very fine book "Test Driven Development for Embedded C" by James W. Grenning.
http://www.amazon.com/Driven-Development-Embedded-Pragmatic-Programmers/dp/193435662X